console.log("Function statement example:");
function sayHello(_name){
    console.log(`Hello, ${_name}`);
}
sayHello("Liz");

console.log("\nFunction definition expression example:");

var factorial = function(num){
    if(num>0){
        return num==1?1:num*factorial(num-1);
    }
    else if(num==0){
        return 1;
    }
    else {
        return -1;
    }
}
var number = 4;
console.log(`${number}! = ${factorial(number)}`);


console.log("\nClosures example:");
var createPerson = function(_name, _age){
    return {
        setName: function(newName){
            _name = newName;
        },

        setAge: function(newAge){
            _age = newAge;
        },

        getName: function(){
            return _name;
        },

        getAge: function(){
            return _age
        }
    }
}

var person = createPerson('Albert', 14);
console.log(`Name: ${person.getName()}`);
console.log(`Name: ${person.getAge()}`);
person.setName("Lily");
person.setAge(22);
console.log(`Name: ${person.getName()}`);
console.log(`Name: ${person.getAge()}`);


console.log("\nAnonymous function example:");

function _expression(a,b,f){
    return f(a,b);
}

console.log(`Result: ${_expression(4,2,(x,y)=>{return x+y;})}`);
